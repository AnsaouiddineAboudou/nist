(function () {
	'use strict';
	app.controller(
			'BasicController', [
			                    '$scope', '$TreeDnDConvert', 'DataDemo', '$http', '$window', function ($scope, $TreeDnDConvert, DataDemo, $http, $window) {
			                    	var tree;

			                    	$scope.tree_data = {};
			                    	$scope.my_tree = tree = {};
			                    	$scope.construct = {};
			                    	$scope.showtable = false;
			                    	$scope.currentId = 0;
			                    	$scope.hasProfile = false;
			                    	$scope.jsonResult = {};
			                    	$scope.hasRoot = false;
			                    	$scope.IsGroup = false;
			                    	$scope.isLoading = true;
			                    	$scope.hasNewColumn = false;
			                    	$scope.profiles = [];
			                    	
			                    	$scope.generate = function() {
			                    		if (angular.isDefined($scope.jsonResult.matrix)
			                    				&& angular.isDefined($scope.jsonResult.profileName)
			                    				&& angular.isDefined($scope.jsonResult.matrix.rows)
			                    				&& angular.isDefined($scope.jsonResult.matrix.columns)) {
		                    		          var last = $scope.jsonResult.matrix.columns.length;
		                    		          for (var p=0; p<last; p++) {
		                    		        	  if ($scope.jsonResult.matrix.columns[p].displayName == "Edit") {
		                    		        		  $scope.jsonResult.matrix.columns[p].cellTemplate = "";
		                    		        	  }
		                    		          }
				                    		$http({
				                    			method: 'POST',
				                    			url: "processing",
				                    			data: $scope.jsonResult,
				                    			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				                    		}).then(function successCallback(response) {
				                    			$window.location.href = '/MatrixIIS';
				                    		}, function errorCallback(response) {
				                    		});
			                    		}
			                    	};

			                    	$scope.createFirst = function(profil) {
			                    		$scope.hasProfile = true;
			                    		$scope.newProfile = false;
	                    				$scope.profiles.push(
	                    					{
	                    						name : profil
	                    					}
	                    				);
	                    				$scope.selectedProfile = $scope.profiles[$scope.profiles.length-1];
	                    				$scope.isLoading = false;
	                    				$scope.jsonResult = {};
	                    				$scope.jsonResult.profileName = profil;
	                    				$scope.jsonResult.matrix = {};
	                    				$scope.jsonResult.matrix.columns = $scope.initCol;
	                    		        for (var p=0; p<$scope.jsonResult.matrix.columns.length; p++) {
	                    		        	  if ($scope.jsonResult.matrix.columns[p].displayName == "Edit") {
	                    		        		  $scope.jsonResult.matrix.columns[p].cellTemplate = choose();
	                    		        	  }
	                    		        }
	                    				$scope.jsonResult.matrix.rows = $scope.initRow;
	                    				var inter = angular.copy($scope.jsonResult);
	                    				var last = inter.matrix.columns.length;
		                    		    for (var p=0; p<last; p++) {
		                    		       if (inter.matrix.columns[p].displayName == "Edit") {
		                    		          inter.matrix.columns[p].cellTemplate = "";
		                    		       }
		                    		    }
			                    		$http({
			                    			method: 'POST',
			                    			url: "processing",
			                    			data: inter,
			                    			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			                    		}).then(function successCallback(response) {
			                    		}, function errorCallback(response) {
			                    		});
			                    	};

			                    	$scope.updatePro = function(profile) {
		                    			  $http.get('api?name=' + profile)
		                    		       .then(function(res){
		                    		          $scope.jsonResult = res.data;
		                    		          var last = $scope.jsonResult.matrix.columns.length;
		                    		          for (var p=0; p<last; p++) {
		                    		        	  if ($scope.jsonResult.matrix.columns[p].displayName == "Edit") {
		                    		        		  $scope.jsonResult.matrix.columns[p].cellTemplate = choose();
		                    		        	  }
		                    		          }
		                    		          $scope.isLoading = false;
		                    		        });
			                    	};
			                    	
			                    	$scope.process = function(node,data) {
			                    		if (angular.isDefined($scope.__proto__.jsonResult.matrix)) {
				                    		if (node._type_ == 'init') {
				                    			var newSeg = null;
				                    			if ($scope.IsGroup) {
				                    				newSeg = createSegment(data, 'group');
				                    			} else {
				                    				newSeg = createSegment(data, 'segment');
				                    			}
				                    			$scope.IsGroup = false;
				                    			tree.add_node_root(newSeg);
				                    		} else if (node._type_ == 'segment') {
				                    			tree.add_node(node, createSubSegment(data, node.DemographicId, "field"));
				                    		} else if (node._type_ == 'field') {
				                    			tree.add_node(node, createSubSegment(data, node.DemographicId, "component"));
				                    		} else if (node._type_ == 'component') {
				                    			tree.add_node(node, createSubSegment(data, node.DemographicId, "subcomponent"));
				                    		} else if (node._type_ == 'group') {
				                    			if ($scope.IsGroup) {
				                    				tree.add_node(node, createSubSegment(data, node.DemographicId, "group"));
				                    			} else {
				                    				tree.add_node(node, createSubSegment(data, node.DemographicId, "segment"));
				                    			}
				                    			$scope.IsGroup = false;
				                    		}
				                    			$scope.__proto__.jsonResult.matrix.rows = tree.getAllNodes();
				                    			reload();
			                    		} else {
			                    			$window.alert("You need to create a profile first !");
			                    		}
			                    		
			                    		$scope.hasClicked = false;
			                    	};

			                    	$scope.test = function() {
			                    		if (!$scope.__proto__.isLoading) {
			                    			$scope.col_defs = $scope.__proto__.jsonResult.matrix.columns;
			                    			tree.setData($scope.__proto__.jsonResult.matrix.rows);
			                    			reload();
			                    			$scope.__proto__.isLoading = true;
			                    			return true;
			                    		}
			                    		return true;
			                    	};
			                    	
			                    	var createSubSegment = function(name, parentID, type) {
			                    		var temp = createSegment(name, type);
			                    		temp['ParentId'] = parentID;
			                    		return temp;
			                    	};
			                    	
			                    	$scope.AddColumn = function() {
			                    		$scope.hasNewColumn = true;
			                    	};
			                    	
			                    	$scope.submitNewColumn = function(name) {
			                    		var temp = $scope.col_defs.pop();
			                    		$scope.col_defs.push(
			                    				{
					                    			   id : Date.now(),
					                    			   field : name,
		                    	                	   displayName:  name,
			                    				});
			                    		$scope.col_defs.push(temp);
			                    		$scope.__proto__.jsonResult.matrix.columns = $scope.col_defs;
			                    		var nodes = tree.getAllNodes();
			                    		var len = nodes.length;
			                    		tree.addColumn(name, len, true, nodes);
			                    		reload();
			                    		$scope.hasNewColumn = false;
			                    	};

			                    	$scope.add = function(node, type) {
			                    		if (type == 'group') {
			                    			$scope.IsGroup = true;
			                    		}
			                    		$scope.currentId = node.DemographicId;
			                    		$scope.hasClicked = true;
			                    		$scope.hasNewSegment = true;
			                    	};

			                    	var reload = function () {
			                    		tree.reload_data();
			                    	};

			                    	$scope.createProfile = function() {
			                    		$scope.newProfile = true;
			                    	};
			                    	
			                    	$scope.remove = function (node) {
			                    		tree.remove_node(node); 
			                    		$scope.__proto__.jsonResult.matrix.rows = tree.getAllNodes();
			                    	};
			                    	
			                    	var createSegment = function(name, type) {
			                    		var temp = {};
			                    		temp['DemographicId'] = Date.now();
			                    		temp['ParentId'] = null;
			                    		temp['Location'] = name;
			                    		temp['_type_'] = type;
			                    		angular.forEach($scope.col_defs, function(value, key) {
			                    				  temp[value.displayName] = "0";
			                    		});
			                    		return temp;
			                    	};

			                    	$scope.my_tree.addFunction = function (node) {
			                    		tree.add_node_root();
			                    		console.log(node);
			                    	};



			                    	var choose = function() {             	
			                    		$http.get("editColumn.html")
			                    		.success(function(response){
			                    			$scope.resp = response;
			                    		});
			                    		return $scope.resp;
			                    	};
			                    	
			                    	$scope.initCol = [
			                    	                   {
			                    	                	   id : 1,
			                    	                	   field:        'Cardinality',
			                    	                	   displayName:  'Cardinality',
			                    	                   },
			                    	                   {
			                    	                	   id : 2,
			                    	                	   field:        'length',
			                    	                	   displayName:  'length',
			                    	                   },
			                    	                   {
			                    	                	   id : 3,
			                    	                	   field:        'Format',
			                    	                	   displayName:  'Format',
			                    	                   },
			                    	                   {
			                    	                	   id : 4,
			                    	                	   field:        'Coded-Element',
			                    	                	   displayName:  'Coded-Element',
			                    	                   },
			                    	                   {
			                    	                	   id : 5,
			                    	                	   field:        'ValueSet',
			                    	                	   displayName:  'ValueSet',
			                    	                   },
			                    	                   {
			                    	                	        
			                    	                	   id : 6,
			                    	                	   field:        'R_USAGE',
			                    	                	   displayName:  'R_USAGE',
			                    	                   },
			                    	                   {
			                    	                	   id : 7,
			                    	                	   field:        'RE_USAGE',
			                    	                	   displayName:  'RE_USAGE',
			                    	                   },
			                    	                   {
			                    	                	   
			                    	                	   id : 8,
			                    	                	   field:        'X_USAGE',
			                    	                	   displayName:  'X_USAGE',
			                    	                   },
			                    	                   {
			                    	                	   id : 9,
			                    	                	   field:        'Predicate-Failure',
			                    	                	   displayName:  'Predicate-Failure',
			                    	                   },
			                    	                   {
			                    	                	   id : 10,
			                    	                	   field:        'Constraint-Failure',
			                    	                	   displayName:  'Constraint-Failure',
			                    	                   },
			                    	                   {
			                    	                	   id : 11,
			                    	                	   displayName:  'Edit',
			                    	                	   cellTemplate: choose(),
			                    	                   }];
			                    	
			                    	$scope.initRow = [{
	                    				'DemographicId': 1,
	                    				'ParentId' : null,
	                    				'Location' : '',
	                    				'Cardinality' : '',
	                    				'length' : '',
	                    				'Format' : '',
	                    				'Coded-Element' : '',
	                    				'ValueSet' : '',
	                    				'R_USAGE' : '',
	                    				'RE_USAGE' : '',
	                    				'X_USAGE' : '',
	                    				'Predicate-Failure' : '',
	                    				'Constraint-Failure' : '',
	                    				_type_ : "init",
	                    			}];
			                    	
			                    	$scope.col_defs = angular.copy($scope.initCol);
			                    	
			                    	$scope.expanding_property = {
			                    			field: 'Location',
			                    			titleClass:  'text-center',
			                    			cellClass:   'v-middle',
			                    			displayName: 'Location',
			                    			titleStyle:   {
			                    				'width': '150pt',
			                    				'text-align' : 'center',
			                    			}

			                    	};
			                    	
			                    	$scope.isLoadingFunct = function() {
			                    		return $scope.__proto__.isLoading;
			                    	};
			                    	
			                    	$scope.init = function(allmatrix) {
			                    		if (Object.keys(allmatrix).length != 0) {
			                    			  $scope.isLoading = true;
			                    			  for (var i = 0; i < allmatrix.filename.length ; i++) {
			                    				  $scope.profiles.push(
			                    						  {
			                    							  name : allmatrix.filename[i]}
			                    						  );
			                    			  }
			                    			  $scope.selectedProfile = $scope.profiles[0];
			                    			  $http.get('api?name=' + allmatrix.filename[0])
			                    		       .then(function(res){
			                    		          $scope.jsonResult = res.data;
			                    		          var last = $scope.jsonResult.matrix.columns.length;
			                    		          for (var p=0; p<last; p++) {
			                    		        	  if ($scope.jsonResult.matrix.columns[p].displayName == "Edit") {
			                    		        		  $scope.jsonResult.matrix.columns[p].cellTemplate = choose();
			                    		        	  }
			                    		          }
			                    		          $scope.isLoading = false;
			                    		        });
			                    			$scope.hasProfile = true;
			                    		} else {
			                    			$scope.transfer = [{
			                    				'DemographicId': 1,
			                    				'ParentId' : null,
			                    				'Location' : '',
			                    				'R_USAGE' : '',
			                    				'RE_USAGE' : '',
			                    				'X_USAGE' : '',
			                    				'Cardinality' : '',
			                    				'length' : '',
			                    				'Format' : '',
			                    				'Coded-Element' : '',
			                    				'ValueSet' : '',
			                    				'Predicate-Failure' : '',
			                    				'Constraint-Failure' : '',
			                    				_type_ : "init",
			                    			}];
			                    		}
			                    	};
			                    	
		                    		$scope.tree_data = $TreeDnDConvert.line2tree($scope.transfer, 'DemographicId', 'ParentId');
			                    }]

	);
})();
