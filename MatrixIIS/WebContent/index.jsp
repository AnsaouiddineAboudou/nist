<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Conformance matrix editor</title>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.1/angular.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.1/angular-route.min.js"></script>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/framework/vendor/bootstrap-css/css/bootstrap.min.css">
<script
	src="${pageContext.request.contextPath}/dist/ng-tree-dnd.debug.js"></script>
<script
	src="${pageContext.request.contextPath}/framework/vendor/prism.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/dist/ng-tree-dnd.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/framework/vendor/prism.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/framework/demo-framework.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/styles/multi.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/styles/main.css"
	media="screen" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/styles/jquery.css"
	media="screen" />
<link href="${pageContext.request.contextPath}/styles/bootstrap.min.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath}/styles/style.css"
	rel="stylesheet">
<script
	src="${pageContext.request.contextPath}/framework/demo-framework.js"></script>
<script
	src="${pageContext.request.contextPath}/framework/view-source.js"></script>
<script src="${pageContext.request.contextPath}/basic/basic.js"></script>

</head>

<body ng-app="TreeDnDTest" ng-controller="BasicController" >
	<div id="wrapper" class="clearfix" ng-init="init(${allMatrix})" >
		<div class="logo" ng-include src="'header.html'"></div>
		<br/><br/>
		
		<div>
			<div ng-view style="display: inline-block;"></div>
			<button ng-click="generate()">Save</button>
			<br />
		</div>
		<div class="footer" ng-include src="'footer.html'"></div>
	</div>
</body>
</html>