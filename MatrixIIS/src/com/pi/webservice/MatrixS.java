package com.pi.webservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import com.pi.DAO.MatrixDAO;
import com.pi.DAO.MatrixDAOImpl;
import com.pi.servlets.Matrix;

@WebService
public class MatrixS {
	
	@Resource
	private WebServiceContext context;
	
	public String getMatrix(String matrixName) throws IOException {
		HttpServletRequest request =
	            (HttpServletRequest) context.getMessageContext().get(MessageContext.SERVLET_REQUEST);
		MatrixDAO matrix = new MatrixDAOImpl(request);
		return matrix.getMatrixByName(matrixName);
	}
}
