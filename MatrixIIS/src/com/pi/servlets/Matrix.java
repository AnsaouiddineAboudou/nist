package com.pi.servlets;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.*;

import com.pi.DAO.MatrixDAO;
import com.pi.DAO.MatrixDAOImpl;
import com.pi.DARequest.ExtractDARequest;
import com.pi.DARequest.ExtractDARequestImpl;

public class Matrix extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ExtractDARequest extract = new ExtractDARequestImpl(req);
		MatrixDAO matrix = new MatrixDAOImpl(req);
		matrix.save(extract.getReader());
	}

}
