package com.pi.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pi.upload.UploadProcessManager;
import com.pi.upload.UploadProcessManagerImpl;


public class Upload extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UploadProcessManager uploadProcessManager = new UploadProcessManagerImpl(request , response);
		uploadProcessManager.process();
		response.sendRedirect("/MatrixIIS");
	}
}