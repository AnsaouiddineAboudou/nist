package com.pi.upload;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.pi.DAO.MatrixDAOImpl;

public class UploadProcessManagerImpl implements UploadProcessManager {

	private HttpServletRequest req;
	private HttpServletResponse resp;

	public UploadProcessManagerImpl(HttpServletRequest req, HttpServletResponse resp) {
		this.req = req;
		this.resp = resp;
	}

	@Override
	public void process() {
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		try {
			List<FileItem> fields = upload.parseRequest(this.req);
			Iterator<FileItem> it = fields.iterator();
			if (!it.hasNext()) {
				//pas de fichier chargé
				return;
			}
			while (it.hasNext()) {
				FileItem fileItem = it.next();
				final UploadDTO uploadDTO = new UploadDTO();
				boolean isFormField = fileItem.isFormField();
				if (!isFormField) {
					uploadDTO.setContents(fileItem.getString());
					uploadDTO.setFieldName(fileItem.getFieldName());
					uploadDTO.setName(fileItem.getName());
					uploadDTO.setType(fileItem.getContentType());
					uploadDTO.setSize(fileItem.getSize());
					PrintWriter out;
					try {
						out = resp.getWriter();
						//out.println(uploadDTO.getContents());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					Thread a = new Thread(){
						public void run() {
							parse(uploadDTO);
						}
					};
					a.start();
					try {
						a.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
	}

	public void parse(UploadDTO uploadDTO) {
		SAXBuilder builder = new SAXBuilder();
		try {
			BufferedWriter writer = null;
			writer = new BufferedWriter(new FileWriter( "json"));
			Document document = (Document) builder.build(new StringReader(uploadDTO.getContents()));
			writer.write("{\"profileName\":\"VXU-Z22\",");
			org.jdom2.Element rootNode = document.getRootElement();
			List list = rootNode.getChildren("Segments");
			List list0 = rootNode.getChild("Messages").getChild("Message").getChildren();
			org.jdom2.Element root = (org.jdom2.Element) list.get(0);
			List list2 = root.getChildren("Segment");
			List list4 = rootNode.getChild("Datatypes").getChildren("Datatype");
			int id = 2;
			Map<String, org.jdom2.Element> datatypeMap = new HashMap <>();
			Map<String, org.jdom2.Element> segmentMap = new HashMap <>();
			for(int i = 0;i<list4.size();i++){
				org.jdom2.Element node = (org.jdom2.Element) list4.get(i);
				datatypeMap.put(node.getAttributeValue("ID"), node);
			}
			for(int i = 0;i<list0.size();i++){
				org.jdom2.Element node = (org.jdom2.Element) list0.get(i);
				explore(node,segmentMap);
			}
			writer.write("\"matrix\":{");
			writer.write("\"columns\": [{\"id\": 10,\"field\": \"R_Usage\",\"displayName\": \"R_Usage\"},{\"id\": 11,\"field\": \"RE_Usage\",\"displayName\": \"RE_Usage\"},{\"id\": 12,\"field\": \"X_Usage\",\"displayName\": \"X_Usage\"},{\"id\": 1,\"field\": \"Cardinality\",\"displayName\": \"Cardinality\"},{\"id\": 2,\"field\": \"Length\",");
			writer.write("\"displayName\": \"Length\"},{\"id\": 3,\"field\": \"Format\",\"displayName\": \"Format\"},{\"id\": 4,\"field\": \"Coded-Element\",\"displayName\": \"Coded-Element\"},");
			writer.write("{\"id\": 5,\"field\": \"ValueSet\",\"displayName\": \"ValueSet\"},{\"id\": 6,\"field\": \"Predicate-Failure\",\"displayName\": \"Predicate-Failure\"},");
			writer.write("{\"id\": 7,\"field\": \"Constraint-Failure\",\"displayName\": \"Constraint-Failure\"},{\"id\" : 8, \"displayName\":  \"Edit\", \"cellTemplate\": \"\"}],");
			writer.write("\"rows\":[");
			for (int i = 0; i < list2.size(); i++) {
				org.jdom2.Element node = (org.jdom2.Element) list2.get(i);
				int parentid = id;
				if (i == 0) {
					writer.write(writeFirst());
				}
				org.jdom2.Element msg = segmentMap.get(node.getAttributeValue("ID"));
				writer.write("{\"DemographicId\":"+id+",");
				writer.write("\"ParentId\":null,");
				writer.write("\"Location\":\""+node.getAttributeValue("Name")+" : "+node.getAttributeValue("Description")+"\",");
				if(msg!=null){
					if(msg.getAttributeValue("Usage").equalsIgnoreCase("R")){
						writer.write("\"R_Usage\":\"3\",");
						writer.write("\"RE_Usage\":\"-1\",");
						writer.write("\"X_Usage\":\"-1\",");
					}
					else if(msg.getAttributeValue("Usage").equalsIgnoreCase("RE")){
						writer.write("\"R_Usage\":\"-1\",");
						writer.write("\"RE_Usage\":\"1\",");
						writer.write("\"X_Usage\":\"-1\",");
					}
					else if(msg.getAttributeValue("Usage").equalsIgnoreCase("X")){
						writer.write("\"R_Usage\":\"-1\",");
						writer.write("\"RE_Usage\":\"-1\",");
						writer.write("\"X_Usage\":\"2\",");
					}
					else{
						writer.write("\"R_Usage\":\"-1\",");
						writer.write("\"RE_Usage\":\"-1\",");
						writer.write("\"X_Usage\":\"-1\",");
					}
				}
				else{
					writer.write("\"R_Usage\":\"-1\",");
					writer.write("\"RE_Usage\":\"-1\",");
					writer.write("\"X_Usage\":\"-1\",");
				}
				if(msg!=null && msg.getAttributeValue("Min")!=null && msg.getAttributeValue("Max")!=null){
					writer.write("\"Cardinality\":\"3\",");
				}
				else{
					writer.write("\"Cardinality\":\"-1\",");
				}
				writer.write("\"Length\":\"-1\",");
				writer.write("\"Format\":\"-1\",");
				writer.write("\"Coded-Element\":\"-1\",");
				writer.write("\"ValueSet\":\"-1\",");
				writer.write("\"Predicate-Failure\":\"-1\",");
				writer.write("\"Constraint-Failure\":\"-1\",");
				writer.write("\"__expanded__\": false,");
				writer.write("\"__inited__\": true,");
				writer.write("\"_type_\":\"segment\"");
				id++;
				List list3 = node.getChildren("Field");
				if(list3.size()>0){
					writer.write(",\"__children__\":[");
				}
				int tag = 1;
				for(int j = 0;j<list3.size();j++){
					org.jdom2.Element node2 = (org.jdom2.Element) list3.get(j);
					int compId = id;
					writer.write("{\"DemographicId\":"+id+",");
					writer.write("\"ParentId\":"+parentid+",");
					writer.write("\"Location\":\""+node.getAttributeValue("Name")+"-"+tag+" : "+node2.getAttributeValue("Name")+"\",");
					if(node2.getAttributeValue("Usage").equalsIgnoreCase("R")){
						writer.write("\"R_Usage\":\"3\",");
						writer.write("\"RE_Usage\":\"-1\",");
						writer.write("\"X_Usage\":\"-1\",");
					}
					else if(node2.getAttributeValue("Usage").equalsIgnoreCase("RE")){
						writer.write("\"R_Usage\":\"-1\",");
						writer.write("\"RE_Usage\":\"1\",");
						writer.write("\"X_Usage\":\"-1\",");
					}
					else if(node2.getAttributeValue("Usage").equalsIgnoreCase("X")){
						writer.write("\"R_Usage\":\"-1\",");
						writer.write("\"RE_Usage\":\"-1\",");
						writer.write("\"X_Usage\":\"2\",");
					}
					else{
						writer.write("\"R_Usage\":\"-1\",");
						writer.write("\"RE_Usage\":\"-1\",");
						writer.write("\"X_Usage\":\"-1\",");
					}
					if(node2.getAttributeValue("Min")!=null && node2.getAttributeValue("Max")!=null){
						writer.write("\"Cardinality\":\"3\",");
					}
					else{
						writer.write("\"Cardinality\":\"-1\",");
					}
					if(node2.getAttributeValue("MinLength")!=null && node2.getAttributeValue("MaxLength")!=null){
						writer.write("\"Length\":\"3\",");
					}
					else{
						writer.write("\"Length\":\"-1\",");
					}
					if(node2.getAttributeValue("Datatype")!=null){
						writer.write("\"Format\":\"3\",");
					}
					else{
						writer.write("\"Format\":\"-1\",");
					}
					Pattern p = Pattern.compile("C(W|N)?E(_.*)?");					                                              
					Matcher m = p.matcher(node2.getAttributeValue("Datatype"));
					if(m.matches()){
						writer.write("\"Coded-Element\":\"3\",");
					}
					else{
						writer.write("\"Coded-Element\":\"-1\",");
					}
					if(node2.getAttributeValue("Binding")!=null){
						writer.write("\"ValueSet\":\"3\",");
					}
					else{
						writer.write("\"ValueSet\":\"-1\",");
					}
					if(!node2.getAttributeValue("Usage").equalsIgnoreCase("C")){
						writer.write("\"Predicate-Failure\":\"3\",");
					}
					else{
						writer.write("\"Predicate-Failure\":\"-1\",");
					}
					writer.write("\"Constraint-Failure\":\"3\",");
					writer.write("\"__inited__\": false,");
					writer.write("\"__expanded__\": false,");
					writer.write("\"_type_\":\"field\"");
					id++;
					writer.write(",\"__children__\":[");
					String datatype = node2.getAttributeValue("Datatype");
					int tag2 = 1;
					org.jdom2.Element node3 = datatypeMap.get(datatype);
					if(node3 != null){
						List list5 = node3.getChildren("Component");
						long start4 = System.currentTimeMillis();
						for(int a = 0;a<list5.size();a++){
							org.jdom2.Element node4 = (org.jdom2.Element) list5.get(a);
							int subcompId = id;
							writer.write("{\"DemographicId\":"+id+",");
							writer.write("\"ParentId\":"+compId+",");
							writer.write("\"Location\":\">"+node.getAttributeValue("Name")+"-"+tag+"."+tag2+" : "+node4.getAttributeValue("Name")+"\",");
							if(node4.getAttributeValue("Usage").equalsIgnoreCase("R")){
								writer.write("\"R_Usage\":\"3\",");
								writer.write("\"RE_Usage\":\"-1\",");
								writer.write("\"X_Usage\":\"-1\",");
							}
							else if(node4.getAttributeValue("Usage").equalsIgnoreCase("RE")){
								writer.write("\"R_Usage\":\"-1\",");
								writer.write("\"RE_Usage\":\"1\",");
								writer.write("\"X_Usage\":\"-1\",");
							}
							else if(node4.getAttributeValue("Usage").equalsIgnoreCase("X")){
								writer.write("\"R_Usage\":\"-1\",");
								writer.write("\"RE_Usage\":\"-1\",");
								writer.write("\"X_Usage\":\"2\",");
							}
							else{
								writer.write("\"R_Usage\":\"-1\",");
								writer.write("\"RE_Usage\":\"-1\",");
								writer.write("\"X_Usage\":\"-1\",");
							}
							if(node4.getAttributeValue("Min")!=null && node4.getAttributeValue("Max")!=null){
								writer.write("\"Cardinality\":\"3\",");
							}
							else{
								writer.write("\"Cardinality\":\"-1\",");
							}
							if(node4.getAttributeValue("MinLength")!=null && node4.getAttributeValue("MaxLength")!=null){
								writer.write("\"Length\":\"3\",");
							}
							else{
								writer.write("\"Length\":\"-1\",");
							}
							if(node4.getAttributeValue("Datatype")!=null){
								writer.write("\"Format\":\"3\",");
							}
							else{
								writer.write("\"Format\":\"-1\",");
							}
							Pattern p1 = Pattern.compile("C(W|N)?E(_.*)?");					                                              
							Matcher m1 = p1.matcher(node4.getAttributeValue("Datatype"));
							if(m1.matches()){
								writer.write("\"Coded-Element\":\"3\",");
							}
							else{
								writer.write("\"Coded-Element\":\"-1\",");
							}
							if(node4.getAttributeValue("Binding")!=null){
								writer.write("\"ValueSet\":\"3\",");
							}
							else{
								writer.write("\"ValueSet\":\"-1\",");
							}
							if(!node4.getAttributeValue("Usage").equalsIgnoreCase("C")){
								writer.write("\"Predicate-Failure\":\"3\",");
							}
							else{
								writer.write("\"Predicate-Failure\":\"-1\",");
							}
							writer.write("\"Constraint-Failure\":\"3\",");
							writer.write("\"__inited__\": false,");
							writer.write("\"__expanded__\": false,");
							writer.write("\"_type_\":\"component\"");
							id++;
							int tag3 = 1;
							writer.write(",\"__children__\":[");
							String datatype2 = node4.getAttributeValue("Datatype");
							org.jdom2.Element node5 = datatypeMap.get(datatype2);
							if(node5 != null){
								List list6 = node5.getChildren("Component");
								for(int n = 0;n<list6.size();n++){
									org.jdom2.Element node6 = (org.jdom2.Element) list6.get(n);
									writer.write("{\"DemographicId\":"+id+",");
									writer.write("\"ParentId\":"+subcompId+",");
									writer.write("\"Location\":\">>"+node.getAttributeValue("Name")+"-"+tag+"."+tag2+"."+tag3+" : "+node6.getAttributeValue("Name")+"\",");
									if(node6.getAttributeValue("Usage").equalsIgnoreCase("R")){
										writer.write("\"R_Usage\":\"3\",");
										writer.write("\"RE_Usage\":\"-1\",");
										writer.write("\"X_Usage\":\"-1\",");
									}
									else if(node6.getAttributeValue("Usage").equalsIgnoreCase("RE")){
										writer.write("\"R_Usage\":\"-1\",");
										writer.write("\"RE_Usage\":\"1\",");
										writer.write("\"X_Usage\":\"-1\",");
									}
									else if(node6.getAttributeValue("Usage").equalsIgnoreCase("X")){
										writer.write("\"R_Usage\":\"-1\",");
										writer.write("\"RE_Usage\":\"-1\",");
										writer.write("\"X_Usage\":\"2\",");
									}
									else{
										writer.write("\"R_Usage\":\"-1\",");
										writer.write("\"RE_Usage\":\"-1\",");
										writer.write("\"X_Usage\":\"-1\",");
									}
									if(node6.getAttributeValue("Min")!=null && node6.getAttributeValue("Max")!=null){
										writer.write("\"Cardinality\":\"3\",");
									}
									else{
										writer.write("\"Cardinality\":\"-1\",");
									}
									if(node6.getAttributeValue("MinLength")!=null && node6.getAttributeValue("MaxLength")!=null){
										writer.write("\"Length\":\"3\",");
									}
									else{
										writer.write("\"Length\":\"-1\",");
									}
									if(node6.getAttributeValue("Datatype")!=null){
										writer.write("\"Format\":\"3\",");
									}
									else{
										writer.write("\"Format\":\"-1\",");
									}
									Pattern p11 = Pattern.compile("C(W|N)?E(_.*)?");					                                              
									Matcher m11 = p11.matcher(node4.getAttributeValue("Datatype"));
									if(m11.matches()){
										writer.write("\"Coded-Element\":\"3\",");
									}
									else{
										writer.write("\"Coded-Element\":\"-1\",");
									}
									if(node6.getAttributeValue("Binding")!=null){
										writer.write("\"ValueSet\":\"3\",");
									}
									else{
										writer.write("\"ValueSet\":\"-1\",");
									}
									if(!node6.getAttributeValue("Usage").equalsIgnoreCase("C")){
										writer.write("\"Predicate-Failure\":\"3\",");
									}
									else{
										writer.write("\"Predicate-Failure\":\"-1\",");
									}
									writer.write("\"Constraint-Failure\":\"3\",");
									writer.write("\"__inited__\": false,");
									writer.write("\"__expanded__\": false,");
									writer.write("\"_type_\":\"subcomponent\"}");
									id++;
									tag3++;
									if(n<list6.size()-1){
										writer.write(",");
									}
								}
							}
							writer.write("]");
							writer.write("}");
							if(a<list5.size()-1){
								writer.write(",");
							}
							tag2++;
						}
					}
					writer.write("]");
					writer.write("}");
					if(j<list3.size()-1){
						writer.write(",");
					}
					tag++;
				}
				if(list3.size()>0){
					writer.write("]");
				}
				writer.write("}");
				if(i<list2.size()-1){
					writer.write(",");
				}
			}
			writer.write("]");
			writer.write("}");
			writer.write("}");
			writer.close();
			final String content = new Scanner(new File("json")).useDelimiter("\\Z").next();
			final MatrixDAOImpl mdao = new MatrixDAOImpl(req);
			Thread a = new Thread(){
				public void run() {
					mdao.save(content);
				}
			};
			a.start();
			try {
				a.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
		}


	}
	
	public void explore(org.jdom2.Element node,Map<String,org.jdom2.Element> segmentMap){
		if(node.getName().equalsIgnoreCase("Segment")){
			segmentMap.put(node.getAttributeValue("Ref"), node);
		}
		else if(node.getName().equalsIgnoreCase("Group")){
			List listtemp = node.getChildren();
			for(int j = 0;j<listtemp.size();j++){
				org.jdom2.Element node2 = (org.jdom2.Element) listtemp.get(j);
				explore(node2,segmentMap);
			}
		}
	}

	private String writeFirst() {

		return 	"{\"DemographicId\": 1,"+
				"\"ParentId\" : null,"+
				"\"Location\" : \"\","+
				"\"R_Usage\" : \"\","+
				"\"RE_Usage\" : \"\","+
				"\"X_Usage\" : \"\","+
				"\"Cardinality\" : \"\","+
				"\"Length\" : \"\","+
				"\"Format\" : \"\","+
				"\"Coded-Element\" : \"\","+
				"\"ValueSet\" : \"\","+
				"\"Predicate-Failure\" : \"\","+
				"\"Constraint-Failure\" : \"\","+
				"\"__expanded__\": false,"+
				"\"__inited__\": true,"+
				"\"_type_\" : \"init\"},";
	}


}
