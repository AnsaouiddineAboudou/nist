package com.pi.DAO;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface MatrixDAO {
	public void save(String matrix);
	public String readAll() throws FileNotFoundException, IOException;
	public String readName();
	public String getMatrixByName(String name);
}
