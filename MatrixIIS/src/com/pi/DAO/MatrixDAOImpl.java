package com.pi.DAO;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import com.pi.servlets.Matrix;

public class MatrixDAOImpl implements MatrixDAO{ 

	public final static String FILES = "Files";
	public final static String MATRIXNAME = "matrixName";
	public final static String EXTENSION = ".json";
	public final static String PROFIL_NAME = "profileName";

	private String path;
	
	public MatrixDAOImpl(HttpServletRequest req) {
		this.path = req.getServletContext().getRealPath("/") + MatrixDAOImpl.FILES;
	}
	
	@Override
	public void save(String matrix) {
		JSONObject obj = new JSONObject(matrix);		
		String matriceName = obj.getString(PROFIL_NAME);
		try {
			File directory = new File(path);  
			directory.mkdirs();
			PrintWriter writer = new PrintWriter(path + "/" + matriceName, "UTF-8");
			writer.println(matrix);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String readAll() throws IOException {
		int i = 0;
		File folder = new File(path);
	    folder.mkdirs();
		ArrayList<StringBuilder> arraySb = new ArrayList<StringBuilder>();
		File[] listOfFiles = folder.listFiles();
		for (File file : listOfFiles) {
			if (file.isFile()) {
				BufferedReader br = new BufferedReader(new FileReader(path + "/" + file.getName()));
				try {
					StringBuilder sb = new StringBuilder();
					String line = br.readLine();
					while (line != null) {
						sb.append(line);
						line = br.readLine();
					}
					arraySb.add(sb);
				} finally {
					br.close();
				}
			}
		}
		String jsonReturn = "";
		boolean several = false;
		for (StringBuilder sb : arraySb) {
			if (several) {
				jsonReturn += ",";
			}
			jsonReturn += sb.toString().replaceAll("\"", "&quot;");
			several = true;
		}
		return "[" + jsonReturn + "]";
	}

	@Override
	public String readName() {
		File folder = new File(path);
	    folder.mkdirs();
		File[] listOfFiles = folder.listFiles();
		JSONObject jsonName = new JSONObject();
		for (File file : listOfFiles) {
			if (file.isFile()) {
				jsonName.append("filename", file.getName());
			}
		}
		String jsonReturn = "";
		jsonReturn += jsonName.toString().replaceAll("\"", "&quot;");
		return jsonReturn;
	}

	@Override
	public String getMatrixByName(String name) {
		BufferedReader br;
		StringBuilder sb = new StringBuilder();
		try {
			br = new BufferedReader(new FileReader(path + "/" + name));
			try {
				String line = br.readLine();
				while (line != null) {
					sb.append(line);
					line = br.readLine();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return sb.toString();
	}

}
