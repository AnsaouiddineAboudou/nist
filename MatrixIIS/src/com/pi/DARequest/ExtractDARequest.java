package com.pi.DARequest;

import java.io.IOException;

public interface ExtractDARequest {
	public String getReader() throws IOException;
}
