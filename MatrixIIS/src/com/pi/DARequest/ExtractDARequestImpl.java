package com.pi.DARequest;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.pi.DAO.MatrixDAOImpl;

public class ExtractDARequestImpl implements ExtractDARequest{

	private HttpServletRequest req;
	
	public ExtractDARequestImpl(HttpServletRequest req) {
		this.req = req;
	}
	
	@Override
	public String getReader() throws IOException {
		BufferedReader reader= req.getReader();
		String line = "";
		String op = "";
		while ((line = reader.readLine()) != null) {
			op += line;
		}
		return op;
	}
}
