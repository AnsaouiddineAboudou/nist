package com.pi.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.webservice.Exception_Exception;
import org.webservice.MessageClient;

/**
 * Servlet implementation class IndEHR
 */
@WebServlet("/IndEHR")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static String result = "";
	//static boolean change = false;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		MessageClient mc = new MessageClient("http://localhost:8080/IIS/MessageService");
		BufferedReader reader= request.getReader();
		String line = "";
		String data = "";
		while ((line = reader.readLine()) != null) {
			data += line;
		}
		result = mc.connectivityTest(data);
		JSONObject o = new JSONObject();
		o.put("TESTresp",result);
		response.setContentType("application/json");
		response.getOutputStream().print(o.toString());
		//result = hl7;
		//change=!change;
		
	}

}
