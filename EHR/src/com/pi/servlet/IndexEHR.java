package com.pi.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.webservice.Exception_Exception;
import org.webservice.MessageClient;

import com.google.gson.Gson;

public class IndexEHR extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static String result = "";   
	
    /**
     * @see HttpServlet#HttpServlet()
     * 
     */
    public IndexEHR() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			this.getServletContext().getRequestDispatcher("/index.html").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		MessageClient mc = new MessageClient("http://localhost:8080/IIS/MessageService");
		try {
			BufferedReader reader= request.getReader();
			String line = "";
			String data = "";
			while ((line = reader.readLine()) != null) {
				data += line;
			}
			
			JSONObject dataJson = new JSONObject(data);
			String hl7 = dataJson.getString("hl7");
			String username = dataJson.getString("Username");
			String facilityID = dataJson.getString("FacilityID");
			String password = dataJson.getString("Password");			
			
			result = mc.submitSingleMessage(username,password,facilityID,hl7);
			JSONObject o = new JSONObject();
			o.put("ACK",result);
			response.setContentType("application/json");
		response.getOutputStream().print(o.toString());
			/*ResponseHandler rh = new ResponseHandler();
			rh.setResult("ss");
			String json = new Gson().toJson(rh);
            response.setContentType("application/json");
            response.getWriter().write(json);
			request.setAttribute("result", "Temp"); // set your String value in the attribute
			request.getRequestDispatcher("/index.html").include(request, response);*/

			//IndEHR.result = mc.connectivityTest(hl7);

		} catch (Exception_Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
