
package org.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConnectivityTestResponse_QNAME = new QName("http://webservice.org/", "connectivityTestResponse");
    private final static QName _SubmitSingleMessageResponse_QNAME = new QName("http://webservice.org/", "submitSingleMessageResponse");
    private final static QName _Exception_QNAME = new QName("http://webservice.org/", "Exception");
    private final static QName _SubmitSingleMessage_QNAME = new QName("http://webservice.org/", "submitSingleMessage");
    private final static QName _ConnectivityTest_QNAME = new QName("http://webservice.org/", "connectivityTest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SubmitSingleMessageResponse }
     * 
     */
    public SubmitSingleMessageResponse createSubmitSingleMessageResponse() {
        return new SubmitSingleMessageResponse();
    }

    /**
     * Create an instance of {@link ConnectivityTestResponse }
     * 
     */
    public ConnectivityTestResponse createConnectivityTestResponse() {
        return new ConnectivityTestResponse();
    }

    /**
     * Create an instance of {@link ConnectivityTest }
     * 
     */
    public ConnectivityTest createConnectivityTest() {
        return new ConnectivityTest();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link SubmitSingleMessage }
     * 
     */
    public SubmitSingleMessage createSubmitSingleMessage() {
        return new SubmitSingleMessage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConnectivityTestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.org/", name = "connectivityTestResponse")
    public JAXBElement<ConnectivityTestResponse> createConnectivityTestResponse(ConnectivityTestResponse value) {
        return new JAXBElement<ConnectivityTestResponse>(_ConnectivityTestResponse_QNAME, ConnectivityTestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitSingleMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.org/", name = "submitSingleMessageResponse")
    public JAXBElement<SubmitSingleMessageResponse> createSubmitSingleMessageResponse(SubmitSingleMessageResponse value) {
        return new JAXBElement<SubmitSingleMessageResponse>(_SubmitSingleMessageResponse_QNAME, SubmitSingleMessageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.org/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitSingleMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.org/", name = "submitSingleMessage")
    public JAXBElement<SubmitSingleMessage> createSubmitSingleMessage(SubmitSingleMessage value) {
        return new JAXBElement<SubmitSingleMessage>(_SubmitSingleMessage_QNAME, SubmitSingleMessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConnectivityTest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.org/", name = "connectivityTest")
    public JAXBElement<ConnectivityTest> createConnectivityTest(ConnectivityTest value) {
        return new JAXBElement<ConnectivityTest>(_ConnectivityTest_QNAME, ConnectivityTest.class, null, value);
    }

}
