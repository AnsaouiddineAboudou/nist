package org.webservice;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public class MessageClient implements Message{

	private final Message client;
	
	public MessageClient(String endpoint) {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setServiceClass(Message.class);
		factory.setAddress(endpoint);
		client = (Message) factory.create();
	}
	
	@Override
	public String submitSingleMessage(String arg0, String arg1, String arg2,
			String arg3) throws Exception_Exception {
		// TODO Auto-generated method stub
		return client.submitSingleMessage(arg0, arg1, arg2, arg3);
	}

	@Override
	public String connectivityTest(String arg0) {
		// TODO Auto-generated method stub
		return client.connectivityTest(arg0);
	}

}
