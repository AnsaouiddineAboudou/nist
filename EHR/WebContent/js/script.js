(function() {
	var app = angular.module('app', []);

	app.directive("message", ['$http','$window','general',function( $http, $window,gen,$scope) {
		return {
			restrict: "E",
			templateUrl: "directives_templates/profile.html",
			controller: function($scope,$window) {
				$scope.textareaText = "";
				$scope.Username = "";
				$scope.FacilityID = "";
				$scope.Password = "";
				this.submit = function() {
					gen.loading = true;
					$http({
						method : 'POST',
						url : "EHR",
						data : {
							hl7 : $scope.textareaText,
							Username : $scope.Username,
							FacilityID : $scope.FacilityID,
							Password : $scope.Password
							},
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
						}
					}).then(function successCallback(response) {
						gen.active=0;
						$scope.result=response.data.ACK;
						gen.loading = false;
					  }, function errorCallback(response) {
						  console.log(response);
					  });
				};
				this.test = function() {
					gen.loading = true;
					$http({
						method : 'POST',
						url : "TestEHR",
						data : $scope.testareaText,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
						}
					}).then(function successCallback(response) {
						gen.active=0;
						$scope.result=response.data.TESTresp;
						gen.loading = false;
					  }, function errorCallback(response) {
						  console.log(response);
					  });
				};
				gen.loading = false;
			},
			controllerAs: "Ctrl"
		};
	}]);
	
	app.directive("messageresp", ['$http','$window','general',function( $http, $window,gen,$scope) {
		return {
			restrict: "E",
			templateUrl: "directives_templates/response.html",
			controller: function($scope,$window) {
			},
			controllerAs: "CResp"
		};
	}]);
	
	app.value("general",{
		tab : 1,
		loading : true,
		loading_msg : "Processing...",
		active : 1,
		time : 0
	});
	
	app.controller('GeneralController', ['general', function(general) {
		this.activeT = function(a){
			return general.active === a;
		};
		
		this.activate = function(a){
			general.active = a;
		};
		
		this.isSelected = function(a){
			return general.tab === a;
		};
		
		this.isLoading = function(){
			return general.loading;
		};
		
		this.loadMsg = function(){
			return general.loading_msg;
		};	
	}]);
	
})();