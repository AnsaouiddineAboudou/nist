package org.webservice;

import gov.nist.healthcare.unified.model.EnhancedReport;
import gov.nist.healthcare.unified.model.impl.CategoryGroup;
import gov.nist.healthcare.unified.model.impl.Entry;
import gov.nist.healthcare.unified.model.impl.ModelImpl;

import java.util.ArrayList;

import org.webservice.Assertion.Location;

public abstract class Classification {
	ArrayList<Assertion> assertions = new ArrayList<Assertion>();
	ModelImpl reportModel;

	public Classification(ModelImpl reportModel) {
		super();
		this.reportModel=reportModel;
	}

	public ArrayList<Assertion> getAssertions() {
		return assertions;
	}

	public void setAssertions(ArrayList<Assertion> assertions) {
		this.assertions = assertions;
	}
	
	public ArrayList<Assertion> parse(int classification) {
		if(reportModel.getClassifications().size()==classification){
			return assertions;
		}
		ArrayList<CategoryGroup> categoryGroup = reportModel.getClassifications().get(classification).getCategories();
		for (CategoryGroup itemCategoryGroup : categoryGroup) {
		    ArrayList<Entry> entryGroup = itemCategoryGroup.getEntries();
    		for (Entry itemEntryGroup : entryGroup) {
    			Assertion assertion = new Assertion();
    			assertion.setType(itemEntryGroup.getCategory());
    			Location location = assertion.getLocation();
    			location.setColumn(itemEntryGroup.getColumn());
    			location.setLine(itemEntryGroup.getLine());
    			location.setPath(itemEntryGroup.getPath());
    			assertion.setLocation(location);
    			assertion.setDescription(itemEntryGroup.getDescription());
    			assertions.add(assertion);
    		}
		}
		return assertions;
	}
	
	@Override
	public String toString() {
		return "Classification [assertions=" + assertions + ""+"]";
	}

	public abstract ArrayList<Assertion> parse();
	
}
