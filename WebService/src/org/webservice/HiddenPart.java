package org.webservice;

import java.util.ArrayList;

import gov.nist.healthcare.hl7ws.client.MessageValidationV2SoapClient;
import gov.nist.healthcare.unified.model.EnhancedReport;
import gov.nist.healthcare.unified.model.impl.ModelImpl;

import org.webservice.*;
import org.webservice.conformance.CheckMatrix;

public class HiddenPart {
	
	public String validate(String hl7Message) throws Exception{
		CheckMatrix cm = new CheckMatrix();
		return cm.crossReference(hl7Message);
	}
}
