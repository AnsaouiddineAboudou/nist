package org.webservice.conformance;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Writer;

public class Test {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		CheckMatrix cm = new CheckMatrix();
		String hl7 = "MSH|^~\\&|Test EHR Application|X68||NIST Test Iz Reg|20120701082240-0500||VXU^V04^VXU_V04|NIST-IZ-001.00|P|2.5.1|||ER|AL|||||Z22^CDCPHINVS\n"+"PID|1||D26376273^^^NIST MPI^MR||Snow^Madelynn^Ainsley^^^^L|Lam^Morgan^^^^^M|20070706|F||2076-8^Native Hawaiian or Other Pacific Islander^CDCREC|32 Prescott Street Ave^^Warwick^MA^02452^USA^L||^PRN^PH^^^657^5558563|||||||||2186-5^non Hispanic or Latino^CDCREC\n"+"PD1|||||||||||02^Reminder/Recall - any method^HL70215|||||A|20120701|20120701\n"+"NK1|1|Lam^Morgan^^^^^L|MTH^Mother^HL70063|32 Prescott Street Ave^^Warwick^MA^02452^USA^L|^PRN^PH^^^657^5558563\n"+"ORC|RE||IZ-783274^NDA|||||||I-23432^Burden^Donna^A^^^^^NIST-AA-1^^^^PRN||57422^RADON^NICHOLAS^^^^^^NIST-AA-1^L^^^MD\n"+"RXA|0|1|20120814||33332-0010-01^Influenza, seasonal, injectable, preservative free^NDC|0.5|mL^MilliLiter [SI Volume Units]^UCUM||00^New immunization record^NIP001|7832-1^Lemon^Mike^A^^^^^NIST-AA-1^^^^PRN|^^^X68||||Z0860BB|20121104|CSL^CSL Behring^MVX|||CP|A\n"+"RXR|C28161^Intramuscular^NCIT|LD^Left Arm^HL70163\n"+"OBX|1|CE|64994-7^Vaccine funding program eligibility category^LN|1|V05^VFC eligible - Federally Qualified Health Center Patient (under-insured)^HL70064||||||F|||20120701|||VXC40^Eligibility captured at the immunization level^CDCPHINVS\n"+"OBX|2|CE|30956-7^vaccine type^LN|2|88^Influenza, unspecified formulation^CVX||||||F\n"+"OBX|3|TS|29768-9^Date vaccine information statement published^LN|2|20120702||||||F\n"+"OBX|4|TS|29769-7^Date vaccine information statement presented^LN|2|20120814||||||F\n";
		/*BufferedWriter writer = null;
		writer = new BufferedWriter(new FileWriter( "report"));
		writer.write(cm.crossReference(hl7));
		writer.close();*/
		System.out.println(cm.crossReference(hl7));
		/*String s = "PID[1]-24[1]";
		System.out.println(neat(s));*/
		}
	public static String neat(String s){
		boolean open = false;
		char[] tab = s.toCharArray();
		char[] result = new char[s.length()];
		int j = 0;
		for(int i=0;i<tab.length;i++){
			if(tab[i] == '['){
				open = true;
			}
			else if(tab[i] == ']'){
				open = false;
			}
			else if(!open){
				result[j] = tab[i];
				j++;
			}
		}
		return String.valueOf(result);
		
	}

}
