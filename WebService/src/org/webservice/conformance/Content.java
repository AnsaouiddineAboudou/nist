package org.webservice.conformance;

import java.util.ArrayList;

public class Content {

	private String rowName = "";
	private ArrayList<ColomnValue> row = new ArrayList<ColomnValue>();
	
	public Content(){
		rowName = "";
		row = new ArrayList<ColomnValue>();
	}
	
	public Content(String rowName, ArrayList<ColomnValue> row) {
		this.rowName = rowName;
		this.row = row;
	}

	public ArrayList<ColomnValue> getRow() {
		return row;
	}

	public void setRow(ArrayList<ColomnValue> row) {
		this.row = row;
	}

	public String getRowName() {
		return rowName;
	}

	public void setRowName(String rowName) {
		this.rowName = rowName;
	}

	@Override
	public String toString() {
		return "RowName [rowName=" + rowName + ", row=" + row + "]";
	}

}
