package org.webservice.conformance;

import java.util.ArrayList;

public class ColomnValue {
	
	private String valueName = "";
	private int value = -1;

	public ColomnValue(){
		valueName = "";
		value = -1;
	}
	
	public ColomnValue(String valueName, int value) {
		this.valueName = valueName;
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getValueName() {
		return valueName;
	}

	public void setValueName(String valueName) {
		this.valueName = valueName;
	}

	@Override
	public String toString() {
		return "ColomnValue [valueName=" + valueName + ", value=" + value + "]";
	}
	
}
