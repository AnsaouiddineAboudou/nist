package org.webservice.conformance;

import java.util.ArrayList;

public class MiniMatrix {
	private String matrixName;
	private ArrayList<Content> lines = new ArrayList<Content>();
	
	public MiniMatrix(){
		matrixName = "";
		lines = new ArrayList<Content>();
	}
	public MiniMatrix(String mn, ArrayList<Content> lignes){
		this.setMatrixName(mn);
		this.setLines(lignes);
	}

	public String getMatrixName() {
		return matrixName;
	}

	public void setMatrixName(String matrixName) {
		this.matrixName = matrixName;
	}

	public ArrayList<Content> getLines() {
		return lines;
	}

	public void setLines(ArrayList<Content> lines) {
		this.lines = lines;
	}
}
