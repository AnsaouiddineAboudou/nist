package org.webservice.conformance;

import java.util.ArrayList;

public class RowName {

	ArrayList<String> rowName = new ArrayList<String>();
	ArrayList<ColomnValue> row = new ArrayList<ColomnValue>();
	
	public RowName(ArrayList<String> rowName, ArrayList<ColomnValue> row) {
		super();
		this.rowName = rowName;
		this.row = row;
	}

	public ArrayList<ColomnValue> getRow() {
		return row;
	}

	public void setRow(ArrayList<ColomnValue> row) {
		this.row = row;
	}

	public ArrayList<String> getRowName() {
		return rowName;
	}

	public void setRowName(ArrayList<String> rowName) {
		this.rowName = rowName;
	}

	@Override
	public String toString() {
		return "RowName [rowName=" + rowName + ", row=" + row + "]";
	}

}
