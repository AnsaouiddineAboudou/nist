package org.webservice.conformance;

import gov.nist.healthcare.hl7ws.client.MessageValidationV2SoapClient;
import gov.nist.healthcare.unified.model.EnhancedReport;
import gov.nist.healthcare.unified.model.impl.ModelImpl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.*;
import org.webservice.Alert;
import org.webservice.Assertion;
import org.webservice.Error;
import org.webservice.Warning;
import com.pi.webservice.MatrixS;
import com.pi.webservice.MatrixSService;


public class CheckMatrix {

	private ArrayList<MiniMatrix> confMatrix;
	private boolean noerrors;
	private boolean severe;

	public CheckMatrix() {
		setConfMatrix(new ArrayList<MiniMatrix>());
	}

	public void generateConformanceMatrix(String jsonMatrix, int depth, JSONArray arrayCol, JSONArray arrayRow){

		if(depth==0){
			jsonMatrix = jsonMatrix.replaceAll("&quot;", "\"");
			JSONObject ar = new JSONObject(jsonMatrix);
			JSONObject obj = ar.getJSONObject("matrix");
			arrayCol = obj.getJSONArray("columns");
			arrayRow = obj.getJSONArray("rows");
		}

		for(int j = 0; j<arrayRow.length();j++){
			JSONObject objLine = arrayRow.getJSONObject(j);
			if(!objLine.getString("_type_").equals("init")){
				JSONArray children = objLine.getJSONArray("__children__");
				String val = objLine.getString("_type_");
				//System.out.println(val);
				if(!objLine.getString("_type_").equals("segment") && children.length()!=0){
					generateConformanceMatrix(jsonMatrix,depth+1,arrayCol,children);
				}else if(objLine.getString("_type_").equals("segment")){
					MiniMatrix mini = new MiniMatrix();
					String matrixName = objLine.getString("Location").split(" ")[0];
					mini.setMatrixName(matrixName);
					//System.out.println(matrixName);
					ArrayList<Content> lines = new ArrayList<Content>();
					Content fline = new Content();
					fline.setRowName(matrixName);
					ArrayList<ColomnValue> fcol = new ArrayList<ColomnValue>();
					fcol.add(new ColomnValue("R_Usage",Integer.parseInt(objLine.getString("R_Usage"))));
					fcol.add(new ColomnValue("RE_Usage",Integer.parseInt(objLine.getString("RE_Usage"))));
					fcol.add(new ColomnValue("X_Usage",Integer.parseInt(objLine.getString("X_Usage"))));
					fcol.add(new ColomnValue("Cardinality",Integer.parseInt(objLine.getString("Cardinality"))));
					fcol.add(new ColomnValue("Length",Integer.parseInt(objLine.getString("Length"))));
					fcol.add(new ColomnValue("Format",Integer.parseInt(objLine.getString("Format"))));
					fcol.add(new ColomnValue("Coded-Element",Integer.parseInt(objLine.getString("Coded-Element"))));
					fcol.add(new ColomnValue("ValueSet",Integer.parseInt(objLine.getString("ValueSet"))));
					fcol.add(new ColomnValue("Predicate-Failure",Integer.parseInt(objLine.getString("Predicate-Failure"))));
					fcol.add(new ColomnValue("Constraint-Failure",Integer.parseInt(objLine.getString("Constraint-Failure"))));
					fline.setRow(fcol);
					lines.add(fline);
					for(int k = 0; k<children.length();k++){
						Content line = new Content();
						line.setRowName(children.getJSONObject(k).getString("Location"));
						//System.out.println("    "+children.getJSONObject(k).getString("Location"));
						ArrayList<ColomnValue> columns = new ArrayList<ColomnValue>();
						for(int i = 0; i<arrayCol.length()-1;i++){
							ColomnValue column = new ColomnValue();
							String colName = arrayCol.getJSONObject(i).getString("field");
							column.setValueName(colName);
							column.setValue(children.getJSONObject(k).getInt(colName));
							//System.out.println(colName+" : "+children.getJSONObject(k).getInt(colName));
							columns.add(column);
						}
						line.setRow(columns);
						lines.add(line);
						JSONArray child = children.getJSONObject(k).getJSONArray("__children__");
						if(child.length()!=0){
							for(int l = 0; l<child.length();l++){
								Content subline = new Content();
								subline.setRowName(child.getJSONObject(l).getString("Location"));
								//System.out.println("       "+child.getJSONObject(l).getString("Location"));
								ArrayList<ColomnValue> subcolumns = new ArrayList<ColomnValue>();
								for(int i = 0; i<arrayCol.length()-1;i++){
									ColomnValue subcolumn = new ColomnValue();
									String colName = arrayCol.getJSONObject(i).getString("field");
									subcolumn.setValueName(colName);
									subcolumn.setValue(child.getJSONObject(l).getInt(colName));
									subcolumns.add(subcolumn);
									//System.out.println(colName+" : "+child.getJSONObject(l).getInt(colName));
								}
								subline.setRow(subcolumns);
								lines.add(subline);
								JSONArray subchild = child.getJSONObject(l).getJSONArray("__children__");
								if(subchild.length()!=0){
									for(int ll = 0; ll<subchild.length();ll++){
										Content subsubline = new Content();
										subsubline.setRowName(subchild.getJSONObject(ll).getString("Location"));
										//System.out.println("           "+subchild.getJSONObject(ll).getString("Location"));
										ArrayList<ColomnValue> subsubcolumns = new ArrayList<ColomnValue>();
										for(int i = 0; i<arrayCol.length()-1;i++){
											ColomnValue subsubcolumn = new ColomnValue();
											String colName = arrayCol.getJSONObject(i).getString("field");
											subsubcolumn.setValueName(colName);
											subsubcolumn.setValue(subchild.getJSONObject(ll).getInt(colName));
											subsubcolumns.add(subsubcolumn);
											//System.out.println(colName+" : "+child.getJSONObject(l).getInt(colName));
										}
										subsubline.setRow(subsubcolumns);
										lines.add(subsubline);
									}
								}
							}
						}
					}
					mini.setLines(lines);
					this.confMatrix.add(mini);
					//System.out.println("Added matrix "+matrixName);
				}
			}
		}
	}

	public String crossReference(String hl7Message) throws Exception{
		String ACK = "";
		String body = "";
		this.noerrors = true;
		this.severe = false;
		String oid = "2.16.840.1.113883.3.72.2.3.99001";
		MessageValidationV2SoapClient client = new MessageValidationV2SoapClient("http://hit-testing2.nist.gov:8090/hl7v2ws/services/soap/MessageValidationV2");
		String XMLreport = client.validate(hl7Message, oid, null, null);
		EnhancedReport report = EnhancedReport.from("xml",XMLreport);
		ModelImpl reportModel = (ModelImpl) report.to("model");
		Error error = new Error(reportModel);
		Warning warnings = new Warning(reportModel);
		error.parse();
		warnings.parse();
		MatrixSService client2 = new MatrixSService();
		MatrixS matrixs = client2.getMatrixSPort();
		//MatrixClient client2 = new MatrixClient("http://localhost:8080/MatrixIIS/MatrixSService");
		String matrix = matrixs.getMatrix("VXU-Z22");
		this.generateConformanceMatrix(matrix,0,null,null);
		ArrayList<Assertion> Errors = error.getAssertions();
		Errors.addAll(warnings.getAssertions());
		ACK += header();
		for(Assertion assertion : Errors){
			int severity = this.conformance(assertion);
			if(severity>1){
				this.severe = true;
			}
			if(severity>0){
				this.noerrors = false;
				body += errSegment(assertion, severity)+"\n";
			}
		}
		if(this.noerrors || !this.severe){
			ACK += "MSA|AA|" + /*controlId +*/ "|\n";
		}else if(!this.noerrors && this.severe){
			ACK += "MSA|AE|" + /*controlId +*/ "|\n";
		}
		if(this.noerrors){
			ACK += "ERR|||0^Message accepted^HL70357|W||||No problems found in message but this interface does not save or forward the data|\n";
		}
		ACK += body;
		System.out.println(ACK);
		return ACK;
		//return XMLreport;
	}

	public int conformance(Assertion assertion){
		String[] mn = assertion.getLocation().getPath().split("-");
		String segment = mn[0].split("\\[")[0];
		int row = -1;
		int matIndex = -1;
		for(int i = 0;i<this.getConfMatrix().size();i++){
			if(this.getConfMatrix().get(i).getMatrixName().equalsIgnoreCase(segment)){
				matIndex = i;
				break;
			}
		}
		if(matIndex>=0){
			for(int i=0;i<this.getConfMatrix().get(matIndex).getLines().size();i++){
				//System.out.println(neatcomp(this.getConfMatrix().get(matIndex).getLines().get(i).getRowName()).split(" ")[0] + " : " + neat(assertion.getLocation().getPath()));
				if(neatcomp(this.getConfMatrix().get(matIndex).getLines().get(i).getRowName()).split(" ")[0].equalsIgnoreCase(neat(assertion.getLocation().getPath()).trim())){
					row = i;
					break;
				}
			}
		}
		if(row>=0 && matIndex>=0){
			if(assertion.getType().contains("R_USAGE")){
				return this.getConfMatrix().get(matIndex).getLines().get(row).getRow().get(0).getValue();
			}
			else if(assertion.getType().contains("RE_USAGE")){
				return this.getConfMatrix().get(matIndex).getLines().get(row).getRow().get(1).getValue();
			}
			else if(assertion.getType().contains("X_USAGE")){
				return this.getConfMatrix().get(matIndex).getLines().get(row).getRow().get(2).getValue();
			}
			else if(assertion.getType().equals("CODED_ELEMENT")){
				return this.getConfMatrix().get(matIndex).getLines().get(row).getRow().get(6).getValue();
			}
			else if(assertion.getType().equals("VALUE_SET")){
				return this.getConfMatrix().get(matIndex).getLines().get(row).getRow().get(7).getValue();
			}
			else if(assertion.getType().equals("CARDINALITY")){
				return this.getConfMatrix().get(matIndex).getLines().get(row).getRow().get(3).getValue();
			}
			else if(assertion.getType().equals("FORMAT")){
				return this.getConfMatrix().get(matIndex).getLines().get(row).getRow().get(5).getValue();
			}
			else if(assertion.getType().equals("CONSTRAINT_FAILURE")){
				return this.getConfMatrix().get(matIndex).getLines().get(row).getRow().get(9).getValue();
			}
			else if(assertion.getType().equals("LENGTH")){
				return this.getConfMatrix().get(matIndex).getLines().get(row).getRow().get(4).getValue();
			}
			else if(assertion.getType().equals("PREDICATE")){
				return this.getConfMatrix().get(matIndex).getLines().get(row).getRow().get(8).getValue();
			}
		}
		return -1;
	}

	public String neat(String s){
		boolean open = false;
		char[] tab = s.toCharArray();
		char[] result = new char[s.length()];
		int j = 0;
		for(int i=0;i<tab.length;i++){
			if(tab[i] == '['){
				open = true;
			}
			else if(tab[i] == ']'){
				open = false;
			}
			else if(!open){
				result[j] = tab[i];
				j++;
			}
		}
		return String.valueOf(result);
	}

	public String neatcomp(String s){
		char[] tab = s.toCharArray();
		char[] result = new char[s.length()];
		int j = 0;
		for(int i=0;i<tab.length;i++){
			if(tab[i] == '>'){
			}
			else{
				result[j] = tab[i];
				j++;
			}
		}
		return String.valueOf(result);
	}

	public String header(){
		String header = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		String dateString = sdf.format(new Date());
		if(false){
			header += "MSH|^~\\&|||||" + dateString + "||ACK^V04^ACK|" + /*controlId +*/ "|P|2.5.1|\n";
			header += "MSA|AR|" + /*controlId +*/ "|\n";
			header += "ERR|||200^Unsupported message type^HL70357|E||||" + /*problem +*/ "|\n";
		}
		else{
			header += "MSH|^~\\&|||||" + dateString + "||ACK^V04^ACK|" + /*controlId +*/ "|P|2.5.1|\n";
		}
		return header ;

	}

	public String errSegment(Assertion assertion, int severity){
		String errSegment = "";
		String[] mn = assertion.getLocation().getPath().split("-");
		String[] comp = assertion.getLocation().getPath().split("\\.");
		String segment = mn[0].split("\\[")[0];
		errSegment += "ERR||";
		errSegment += segment+ "^";
		//errSegment += conformanceIssue.getErrorLocation().getSegmentSequence().getValue() + "^";
		if(mn.length>1){
			String[] row = mn[1].split("\\[");
			errSegment += row[0] + "^";
		}
		errSegment += 1+ "^";
		if(comp.length>1){
			errSegment += comp[1] + "^";
		}
		if(comp.length>2){
			errSegment += comp[2];
		}
		errSegment += "|";
		//errSegment += conformanceIssue.getHl7ErrorCode().getIdentifier().getValue() + "^";
		//errSegment += conformanceIssue.getHl7ErrorCode().getText().getValue() + "^";
		//errSegment += conformanceIssue.getHl7ErrorCode().getNameOfCodingSystem().getValue();
		if(severity==1){
			errSegment += "|I";
		}
		else if(severity==2){
			errSegment += "|W";
		}
		else if(severity==3){
			errSegment += "|E";
		}
		errSegment += "|";
		//errSegment += conformanceIssue.getApplicationErrorCode().getIdentifier().getValue() + "^";
		//errSegment += conformanceIssue.getApplicationErrorCode().getText().getValue() + "^";
		//errSegment += conformanceIssue.getApplicationErrorCode().getNameOfCodingSystem().getValue();
		errSegment += "|||";
		errSegment += assertion.getDescription();
		errSegment += "|";
		return errSegment;

	}

	public ArrayList<MiniMatrix> getConfMatrix() {
		return confMatrix;
	}

	public void setConfMatrix(ArrayList<MiniMatrix> confMatrix) {
		this.confMatrix = confMatrix;
	}

}
