package org.webservice;

import gov.nist.healthcare.unified.model.impl.ModelImpl;

import java.util.ArrayList;

public class Error extends Classification {

	public Error(ModelImpl reportModel) {
		super(reportModel);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ArrayList<Assertion> parse() {
		//reportModel.getClassifications().get(0).getCategories();
		int index = -1;
		for(int i = 0;i<reportModel.getClassifications().size();i++){
			if(reportModel.getClassifications().get(i).getClassification().equalsIgnoreCase("error")){
				index = i;
			}
		}
		if(index >=0){
		return super.parse(index);
		}
		else{
			return new ArrayList<Assertion>();
		}
	}

}
