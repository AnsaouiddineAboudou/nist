package com.pi.webservice;

import gov.nist.healthcare.hl7ws.client.MessageValidationV2SoapClient;

import org.webservice.HiddenPart;
import org.webservice.conformance.CheckMatrix;


public class Test {

	public static void main(String[] args) throws IOException_Exception {
		// TODO Auto-generated method stub
		MatrixClient client = new MatrixClient("http://localhost:8080/MatrixIIS/MatrixSService");
		System.out.println(client.getAllMatrix());
		String jsonMatrix = client.getAllMatrix();
		CheckMatrix cm = new CheckMatrix();
		cm.generateConformanceMatrix(jsonMatrix);
		System.out.println(cm.getAllRows().get(0).getRow().get(0).getValueName().toString());
	}

}
