package com.pi.webservice;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public class MatrixClient implements MatrixS{
	private final MatrixS client;
	
	public MatrixClient(String endpoint) {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setServiceClass(MatrixS.class);
		factory.setAddress(endpoint);
		client = (MatrixS) factory.create();
	}
	@Override
	public String getAllMatrix() throws IOException_Exception {
		// TODO Auto-generated method stub
		return client.getAllMatrix();
	}

}
