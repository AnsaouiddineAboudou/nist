
package com.pi.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.pi.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetMatrixResponse_QNAME = new QName("http://webservice.pi.com/", "getMatrixResponse");
    private final static QName _IOException_QNAME = new QName("http://webservice.pi.com/", "IOException");
    private final static QName _GetMatrix_QNAME = new QName("http://webservice.pi.com/", "getMatrix");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.pi.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetMatrix }
     * 
     */
    public GetMatrix createGetMatrix() {
        return new GetMatrix();
    }

    /**
     * Create an instance of {@link IOException }
     * 
     */
    public IOException createIOException() {
        return new IOException();
    }

    /**
     * Create an instance of {@link GetMatrixResponse }
     * 
     */
    public GetMatrixResponse createGetMatrixResponse() {
        return new GetMatrixResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMatrixResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.pi.com/", name = "getMatrixResponse")
    public JAXBElement<GetMatrixResponse> createGetMatrixResponse(GetMatrixResponse value) {
        return new JAXBElement<GetMatrixResponse>(_GetMatrixResponse_QNAME, GetMatrixResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.pi.com/", name = "IOException")
    public JAXBElement<IOException> createIOException(IOException value) {
        return new JAXBElement<IOException>(_IOException_QNAME, IOException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMatrix }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.pi.com/", name = "getMatrix")
    public JAXBElement<GetMatrix> createGetMatrix(GetMatrix value) {
        return new JAXBElement<GetMatrix>(_GetMatrix_QNAME, GetMatrix.class, null, value);
    }

}
