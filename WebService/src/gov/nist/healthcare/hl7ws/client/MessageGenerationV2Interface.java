package gov.nist.healthcare.hl7ws.client;

public interface MessageGenerationV2Interface {

	String generate(String oid, String xmlConfig, String xmlGenerationContext);

	String getServiceStatus();

	String loadResource(String resource, String oid, String type);

	String generateFromTemplate(String message, String oid, String xmlConfig,
			String xmlGenerationContext);

}
